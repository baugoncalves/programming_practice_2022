#include<stdio.h>
#include<stdlib.h>

struct Pessoa {
    char nome[30], end[60];
};
typedef struct Pessoa Pessoa;

struct No {
    Pessoa pes;
    struct No *prox;
};

main() {
    struct No *inicio, *fim, *aux;
    char opc;

    inicio = malloc(sizeof(struct No));
    printf("Nome: ");
    __fpurge(stdin);
    scanf("%s", &inicio->pes.nome);
    printf("Endereco: ");
    __fpurge(stdin);
    scanf("%s", &inicio->pes.end);

    inicio->prox = NULL;
    fim = inicio;

    do {
        aux = malloc(sizeof(struct No));
        printf("Nome: ");
        __fpurge(stdin);
        scanf("%s", &aux->pes.nome);
        printf("Endereco: ");
        __fpurge(stdin);
        scanf("%s", &aux->pes.end);

        fim->prox = aux;
        fim = aux;
        aux->prox = NULL;

        printf("Deseja sair? (s/n): ");
        __fpurge(stdin);
        scanf("%c", &opc);
    }while(opc != 's');

    aux = inicio;
    while(aux!=NULL) {
        puts(aux->pes.nome);
        puts(aux->pes.end);
        aux = aux->prox;
    }
}