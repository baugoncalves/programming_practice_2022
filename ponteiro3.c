#include<stdio.h>

void umaFuncao(int p) {
    p = 115;
}

void funcaoComPonteiro(int *p) {
    *p = 115;
}

void maisOutra(int vetor[]) {
    vetor[2] = 230;
}

main() {
    int numero;
    int *ponteiro;
    int v[3];

    numero = 10;
    ponteiro = &numero;
    printf("%d %d\n", numero, *ponteiro);
    numero = 30;
    printf("%d %d\n", numero, *ponteiro);
    *ponteiro = 50;
    printf("%d %d\n", numero, *ponteiro);

    umaFuncao(numero); //passagem de parametro por valor
    printf("%d %d\n", numero, *ponteiro);

    funcaoComPonteiro(&numero); //passagem de parametro por referencia
    printf("%d %d\n", numero, *ponteiro);
    
    v[0] = 70;
    v[1] = 75;
    v[2] = 80;
    maisOutra(v);
    printf("%d\n", v[2]);
}