#include<stdio.h>
#include<stdlib.h>

struct Pessoa {
    char nome[30],email[80];
    int idade;
};
typedef struct Pessoa Pessoa;

main() {
    Pessoa *pes;

    /* sizeof retorna a quantidade de bytes de um tipo, neste caso, a 
    quantidade de bytes de Pessoa*/

    // pes = malloc(sizeof(Pessoa) * 3);
    pes = calloc(3, sizeof(Pessoa));
    for (int i=0; i < 3; i++) {
       printf("Nome: ");
        __fpurge(stdin); // no windows - fflush(stdin)
        scanf("%s", &pes[i].nome);
        printf("Email: ");
        __fpurge(stdin);
        scanf("%s", &pes[i].email);
        printf("Idade: ");
        scanf("%d", &pes[i].idade);
    }

    for (int i=0; i < 3; i++) {
        printf("Nome: %s\nEmail: %s\n", pes[i].nome, pes[i].email);
        printf("Idade: %d\n", pes[i].idade);
    }
}