#include<stdio.h>

void gravar() {
    int v;
    FILE *f;

    f = fopen("meuarquivo.txt", "a"); //ou a, caso queira manter o conteudo anterior
    do {
        printf("Digite um valor: ");
        scanf("%d", &v);
        fprintf(f, "%d\n", v);
    }while(v!=0);
    fclose(f);
}

void ler() {
    int v;
    FILE *f;

    f = fopen("meuarquivo.txt", "r");
    if (f!=NULL) {
        while(!feof(f)) {   // ou while(feof(f) != 1)
            fscanf(f, "%d", &v);
            if (v != 0) {
                printf("Valor: %d\n", v);
            }
        }
        fclose(f);
    } else {
        printf("Arquivo nao existe\n");
    }
}

main() {
    int opc;

    do {
        printf("===Cadastro de numeros===\n\n");
        printf("1.Novo\n2.Listar\n3.Sair\nDigite uma opcao: ");
        scanf("%d", &opc);
        switch(opc) {
            case 1:
                gravar();
            break;

            case 2:
                ler();
            break;

            case 3:
                printf("Saindo...\n");
            break;

            default:
                printf("opcao invalida\n");
        }
    }while(opc != 3);


    /*
    Tipo de dado: FILE
    Algumas funcoes para manipular arquivos: fopen, fclose, fprintf e fscanf.
    */
}