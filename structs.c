#include<stdio.h>
#include<string.h>
#define QTDE 2

struct Pessoa {
    char name[30], phone[15],email[80];
    int age;
};
typedef struct Pessoa Pessoa;

void printStruct(Pessoa p[QTDE]) {
    for (int i=0; i<QTDE; i++) {
        printf("Name: %s\n", p[i].name);
        printf("Phone: %s\n", p[i].phone);
        printf("Email: %s\n", p[i].email);
        printf("Idade: %d\n", p[i].age);
    }
}

main() {
    Pessoa pes[QTDE];
    Pessoa outra;
    Pessoa maisum;

    for (int i=0; i<QTDE; i++) {
        printf("Type your name: ");
        scanf("%s", &pes[i].name);
        printf("Type your phone: ");
        scanf("%s", &pes[i].phone);
        printf("Type your email: ");
        scanf("%s", &pes[i].email);
        printf("Type your age: ");
        scanf("%d", &pes[i].age);
    }
    // printStruct(pes);

    for (int i=0; i<QTDE; i++) {
        printf("%s %d\n", pes[i].name, strlen(pes[i].name));
        if (pes[i].age > 20) {
            printf("Greater than 20: %s %d\n", pes[i].name, pes[i].age);
        }
    }
}