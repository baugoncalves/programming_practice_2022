#include<stdio.h>
#include<stdlib.h>

struct Funcionario {
    char nome[30];
    float salario;

    struct Nascimento {
        int ano,mes,dia;
    } Nascimento;
};
typedef struct Funcionario Funcionario;

main() {
    Funcionario *func;

    func = (Funcionario *) malloc(sizeof(Funcionario));

    printf("Entre com o nome: ");
    scanf("%s", &func->nome);
    printf("Entre com o salario: ");
    scanf("%f", &func->salario);
    printf("Entre com o ano: ");
    scanf("%d", &func->Nascimento.ano);
    printf("Entre com o mes: ");
    scanf("%d", &func->Nascimento.mes);
    printf("Entre com o dia: ");
    scanf("%d", &func->Nascimento.dia);

    printf("Nome: %s\n", func->nome);
    printf("Salario: %.2f\n", func->salario);
    printf("Nascimento: %d/%d/%d\n", func->Nascimento.dia, func->Nascimento.mes,func->Nascimento.ano);

    free(func);
}