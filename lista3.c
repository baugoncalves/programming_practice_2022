#include<stdio.h>
#include<stdlib.h>
#include<strings.h>

struct Pessoa {
    char nome[30], end[60];
};
typedef struct Pessoa Pessoa;

struct No {
    Pessoa pes;
    struct No *prox;
};
typedef struct No *pno;

void iniciar(pno *plista) {
    *plista = NULL;
}

void addInformacao(pno *plista) {
    Pessoa info;

    printf("Nome: ");
    __fpurge(stdin);
    scanf("%s", &info.nome);
     printf("Endereco: ");
    __fpurge(stdin);
    scanf("%s", &info.end);

    inserir(plista, info);
}

void inserir(pno *plista, Pessoa info) {
    pno aux,q;

    aux = (pno) malloc(sizeof(struct No));
    aux->pes = info;
    aux->prox = NULL;
    if (*plista == NULL) {
        *plista = aux;
    } else {
        q=*plista;
        while(q->prox!=NULL) {
            q=q->prox;
        }
        // for(q=*plista; q->prox!=NULL; q=q->prox);
        q->prox = aux;
    }
}

void listar(pno plista) {
    if (plista == NULL) {
        printf("Lista vazia\n");
    } else {
        while(plista != NULL) {
            printf("Nome: %s\nEndereco: %s\n\n", plista->pes.nome, plista->pes.end);
            plista = plista->prox;
        }
    }
}

void pesquisar (pno plista){
    char nome[30];
    int found = 0;

    printf("Digite O Nome A Pesquisar: ");
    fflush(stdin);
    scanf("%s", &nome);
    while (plista!=NULL) {
        if (strcmp(plista->pes.nome, nome) == 0) {
            printf("\nNome: %s\n", plista->pes.nome);
            printf("Endereco: %s\n\n", plista->pes.end);
            found = 1;
        }
        plista=plista->prox;
    }
    if (found == 0){
        printf ("Nome não encontrado!\n\n");
    }
}

void remover(pno *plista) {
    pno p, aux;
    char nome[30];

    aux = NULL;
    p = *plista;
    printf("Nome a remover: ");
    __fpurge(stdin);
    scanf("%s", &nome);
    while(p!=NULL) {
        if(strcmp(p->pes.nome, nome) == 0) {
            p = p->prox;
            if (aux == NULL) {
                free(*plista);
                *plista = p;
            } else {
                free(aux->prox);
                aux->prox = p;
            }
        } else {
            aux = p;
            p = p->prox;
        }
    }
}

void salvar(pno plista) {
    FILE *f;
    Pessoa aux;

    f = fopen("lista.txt", "w");
    while(plista != NULL) {
        aux = plista->pes;
        fwrite(&aux, sizeof(Pessoa), 1, f);
        plista = plista->prox;
    }
    fclose(f);
}

void carregar(pno *plista) {
    FILE *f;
    Pessoa aux;
    
    f = fopen("lista.txt", "r");
    if (f == NULL) {
         f = fopen("lista.txt", "w");
         fclose(f);
         f = fopen("lista.txt", "r");
    }
    while(!feof(f)) {
        fread(&aux, sizeof(Pessoa), 1, f);
        if(!feof(f)) {
            inserir(plista, aux);
        }
    }
    fclose(f);
}

main() {
    pno plista;
    int op;

    iniciar(&plista);
    carregar(&plista);

    do {
        printf("1.Inserir\n2.Listar\n3.Pesquisar\n4.Remover\n");
        printf("5.Sair\n");
        printf("Digite uma opcao: ");
        scanf("%d", &op);

        switch (op) {
        case 1:
            addInformacao(&plista);
            break;
        
        case 2:
            listar(plista);
            break;

        case 3:
            pesquisar(plista);
            break;

        case 4:
            remover(&plista);
            break;

        case 5:
            printf("Saindo...");
            salvar(plista);
            break;

        default:
            printf("Opcao invalida\n\n");
            break;
        }
    } while(op != 5);
}