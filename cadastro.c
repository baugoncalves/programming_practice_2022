#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Pessoa {
    char nome[30],email[80];
    int idade;
};
typedef struct Pessoa Pessoa;

void cadastrar(Pessoa pes[], int *qtde) {
    if (*qtde == 10) {
       printf("O cadastro esta cheio\n\n");
    } else {
        printf("Nome: ");
        __fpurge(stdin); // no windows - fflush(stdin)
        scanf("%[^\n]", &pes[*qtde].nome);
        printf("Email: ");
        __fpurge(stdin);
        scanf("%[^\n]", &pes[*qtde].email);
        printf("Idade: ");
        scanf("%d", &pes[*qtde].idade);
        (*qtde)++;
    }
}

void listar(Pessoa pes[], int qtde) {
    if (qtde == 0) {
        printf("O cadastro esta vazio\n\n");
    } else {
        for(int i=0; i< qtde; i++) {
            printf("Nome: %s\n", pes[i].nome);
            printf("Email: %s\n", pes[i].email);
            printf("Idade: %d\n\n", pes[i].idade);
        }
    }
}

void salvar(Pessoa pes[], int qtde) {
    FILE *f;

    f = fopen("cadastro.txt", "w");
    for (int i=0; i < qtde; i++) {
        // fprintf(f, "%s\n", pes[i].nome);
        // fprintf(f, "%s\n", pes[i].email);
        // fprintf(f, "%d\n", pes[i].idade);
        fwrite(&pes[i], sizeof(Pessoa), 1, f);
    }
    fclose(f);
}

void carregar(Pessoa pes[], int *qtde) {
    FILE *f;
    Pessoa aux;

    f = fopen("cadastro.txt", "r");
    if (f != NULL) {
        while(!feof(f)) {
            // fscanf(f,"%s", &aux.nome);
            // fscanf(f,"%s", &aux.email);
            // fscanf(f,"%d", &aux.idade);
            fread(&aux, sizeof(Pessoa), 1, f);
            if(!feof(f)) {
                pes[*qtde] = aux;
                (*qtde)++;
            }
        }
        fclose(f);
    }
}

main() {
    int opc, qtde=0;
    Pessoa cad[10];

    carregar(cad, &qtde);
    do {
        printf("===Cadastro de Pessoas===\n\n");
        printf("1.Cadastrar\n2.Listar\n3.Sair\nDigita uma opcao: ");
        scanf("%d", &opc);

        switch (opc) {
            case 1:
                cadastrar(cad, &qtde);
                salvar(cad, qtde);
                break;

            case 2:
                listar(cad, qtde);
                break;

            case 3:
                printf("Saindo...\n\n");
                break;
            
            default:
                printf("Opcao invalida\n\n");
                break;
        }
    }while(opc!=3);
}