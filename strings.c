#include<stdio.h>
#include<string.h>

main() {
    char name[30];
    int count;

    printf("Type a name: ");
    scanf("%s", &name);
    // fflush(stdin);  //for windows
    // __fpurge(stdin);  //for linux
    // gets(name);
    // scanf("%[^\n]", &name);
    // fgets(name, 30, stdin);

    printf("Name: %s\n", name);
    // puts(name);
    printf("Quantidade de letras: %d\n", strlen(name));
    printf("Segunda letra: %c\n", name[1]);

    count = 0;
    for(int i=0; i < strlen(name); i++) {
        if (name[i] == 'a' || name[i] == 'A') {
            count++;
        }
    }
    printf("Quantidade de letras A: %d\n", count);
}