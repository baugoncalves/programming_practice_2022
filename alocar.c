#include<stdio.h>
#include<stdlib.h>

main() {
    int *p;

    p = malloc(4);
    *p = 10;

    printf("%d\n", *p);
}


// malloc - server para alocar um espaço em memória
// calloc - idem, a diferença está na sintaxe
// realloc - mudar o tamanho de um espaco alocado
// free - liberar espaco na memoria