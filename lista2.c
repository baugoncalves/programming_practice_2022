#include<stdio.h>

struct Pessoa {
    char nome[30], end[60];
};
typedef struct Pessoa Pessoa;

struct No {
    Pessoa pes;
    struct No *prox;
};
typedef struct No *pno;

pno alocar() {
    return  malloc(sizeof(struct No));
}

// esta funcao serve para comparar se a lista esta vazia
int vazia(pno i) {
    if (i == NULL) {
        return 1;
    }
    return 0;
}

/* inicializa os ponteiros de controle da lista, apontando-os para NULL.
Caso nao tivesse esta funcao, fugiria do meu controle pra onde estes ponteiros 
apontam se nao estivesse nenhum elemento na minha lista.
*/
void inicializa(pno *i, pno *f) {
    *i = NULL;
    *f = NULL;
}

void inserir(pno *i, pno *f) {
    pno aux;

    aux = alocar();
    printf("Nome: ");
    __fpurge(stdin);
    scanf("%s", &aux->pes.nome);
     printf("Endereco: ");
    __fpurge(stdin);
    scanf("%s", &aux->pes.end);

    /*caso a lista este vazia, faco este process, apontando o ponteiro inicio para o
    primeiro elemento da minha lista*/
    if (vazia(*i)) {
        *i = aux;
        *f = aux;
        (*f)->prox = NULL;
    } else {
        (*f)->prox = aux;
        *f = aux;
        (*f)->prox = NULL;
    }
}

void listar(pno i) {
    while(i!=NULL) {
        printf("Nome: %s\n", i->pes.nome);
        printf("Endeerco: %s\n\n", i->pes.end);
        i = i->prox;
    }
}

void pesquisar(pno i) {
    char nome[30];
    int found = 0;

    printf("Nome a pesquisar: ");
    __fpurge(stdin);
    scanf("%s", &nome);
    while(i!=NULL) {
        if(strcmp(i->pes.nome, nome) == 0) {
            printf("\nNome: %s\n", i->pes.nome);
            printf("Endereco: %s\n\n", i->pes.end);
            found = 1;
        }
        i = i->prox;
    }
    if (found == 0) {
        printf("\nNome nao encontrado!\n\n");
    }
}

void removerFim(pno *i, pno *f) {
    pno aux, p;
    
    aux = *i;
    p = NULL;
    if (*i == NULL) {
        printf("A lista esta vazia\n");
    } else {
        if (aux->prox == NULL) {
            *i = NULL;
            *f = NULL;
            free(aux);
        } else {
            while(aux != NULL) {
                if (aux == *f) {
                    free(*f);
                    *f = p;
                    (*f)->prox = NULL;
                } else {
                    p = aux;
                    aux = aux->prox;
                }
            }
        }
    }
}

void removerInicio(pno *i) {
    pno aux, p;

    aux = *i;
    *i = (*i)->prox;
    free(aux);
}


main() {
    int op;
    pno inicio, fim;

    //inicializando os ponteiros de controle da ista
    inicializa(&inicio, &fim);

    do {
        printf("1.Inserir\n2.Listar\n3.Pesquisar\n4.Remover do Fim\n");
        printf("5.Remover do inicio\n6.Sair\n");
        printf("Digite uma opcao: ");
        scanf("%d", &op);

        switch (op) {
        case 1:
            inserir(&inicio, &fim);
            break;
        
        case 2:
            listar(inicio);
            break;

        case 3:
            pesquisar(inicio);
            break;

        case 4:
            removerFim(&inicio, &fim);
            break;

         case 5:
            removerInicio(&inicio);
            break;

        case 6:
            printf("Saindo...");
            break;

        default:
            printf("Opcao invalida\n\n");
            break;
        }
    } while(op != 6);
}