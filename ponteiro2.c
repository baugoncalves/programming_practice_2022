#include<stdio.h>

main() {
   int x,y;
   int *z,*w;

   x = 10;
   y = 15;
   z = &x;
   w = &y;

   printf("%d %d %d %d\n", x, y, *z, *w);

   *z = x + 10;  // z = 20  x = 20
   *w = x + 10;

   printf("%d %d %d %d\n", x, y, *z, *w);

   y = *z - 10;
   x = (*z-10)*5;

   printf("%d %d %d %d\n", x, y, *z, *w);

   w = z;

   printf("%d %d %d %d\n", x, y, *z, *w);

   (*w)--;

   printf("%d %d %d %d\n", x, y, *z, *w);

}