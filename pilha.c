#include<stdio.h>
#include<stdlib.h>

struct No {
    char elemento;
    struct No *prox;
};

struct Pilha {
    struct No *topo;
};
typedef struct Pilha Pilha;

void InicializarPilha(Pilha *p) {
    p->topo = NULL;
}

struct No * alocar() {
    struct No *p;
    p = (struct No *) malloc(sizeof(struct No));
}

int PilhaVazia(Pilha p) {
    if (p.topo == NULL)
        return 1;
    return 0;
}

void Empilhar(Pilha *p, char letra) {
    struct No *aux;

    aux = alocar();
    if (aux == NULL) {
        printf("Memoria insuficiente\n");
    } else {
        aux->elemento = letra;
        aux->prox = p->topo;
        p->topo = aux;
    }
}

char Desempilhar(Pilha *p) {
    struct No *aux;
    char letra;

    if (PilhaVazia(*p)) {
        printf("Pilha vazia\n");
    } else {
        letra = p->topo->elemento;
        aux = p->topo->prox;
        free(p->topo);
        p->topo = aux;
        return letra;
    }
}

void Imprimir(Pilha *p) {
    struct Pilha aux;
    char letra;

    if (PilhaVazia(*p)) {
        printf("Pilha vazia\n");
    } else {
        InicializarPilha(&aux);
        while(p->topo != NULL) {
            letra = Desempilhar(p);
            printf("Elemento: %c\n", letra);
            Empilhar(&aux, letra);
        }
        while(aux.topo != NULL) {
            letra = Desempilhar(&aux);
            Empilhar(p, letra);
        }
    }
}

main() {
    Pilha p;

    InicializarPilha(&p);

    Empilhar(&p, 'A');
    Empilhar(&p, 'M');
    Empilhar(&p, 'U');
    Empilhar(&p, 'R');
    Empilhar(&p, 'Y');

    printf("Impressao 1\n\n");
    Imprimir(&p);

    printf("\n\nImpressao 2\n\n");
    Imprimir(&p);
}