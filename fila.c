#include<stdio.h>
#include<stdlib.h>

struct No {
    char letra;
    struct No *prox;
};

struct Fila {
    struct No *inicio, *fim;
};
typedef struct Fila Fila;

void InicializarFila(Fila *f) {
    f->inicio = NULL;
    f->fim = NULL;
}

int FilaVazia(Fila f) {
    if (f.inicio == NULL)
        return 1;
    return 0;
}

struct No * alocar() {
    struct No *p;
    p = (struct No *) malloc(sizeof(struct No));
    return p;
}

void Enfileirar(Fila *f, char l) {
    struct No *p;

    p = alocar();
    if (p == NULL) {
        printf("Memoria insuficente");
    } else{
        p->letra = l;
        p->prox = NULL;
        if(FilaVazia(*f)) {
            f->inicio = p;
            f->fim = p;
        } else {
            f->fim->prox = p;
            f->fim = p;
        }
    }
}

char Desenfileirar(Fila *f) {
    char l;
    struct No *aux;

    if (FilaVazia(*f)){
        printf("Fila esta vazia");
    }
    aux = f->inicio->prox;
    l = f->inicio->letra;
    free(f->inicio);
    f->inicio = aux;
    if (aux == NULL)
        f->fim = aux;
    return l;
}

void Imprimir(Fila *f) {
    char l;
    struct Fila aux;

    InicializarFila(&aux);
    if (FilaVazia(*f))
        printf("Fila vazia\n");
    else {
        while(!FilaVazia(*f)) {
            l = Desenfileirar(f);
            printf("%c\n", l);
            Enfileirar(&aux, l);
        }
        f->inicio = aux.inicio;
        f->fim = aux.fim;
    }
}


main() {
    Fila f;

    InicializarFila(&f);

    Enfileirar(&f, 'A');
    Enfileirar(&f, 'U');
    Enfileirar(&f, 'L');
    Enfileirar(&f, 'A');

    printf("Primeira impressao\n\n");
    Imprimir(&f);

    Enfileirar(&f, 'B');
    Enfileirar(&f, 'C');

    printf("\nSegunda impressao\n\n");
    Imprimir(&f);

    Desenfileirar(&f);
    Desenfileirar(&f);
    Desenfileirar(&f);

    printf("\nTerceira impressao\n\n");
    Imprimir(&f);
}